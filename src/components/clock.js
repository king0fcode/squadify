import React, { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "jquery/dist/jquery.min";
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseOpen: false,
      modalSearch: false,
      color: "navbar-transparent"
    };
  }

  render() {
    return (
      <>
        <div className="col col-12 text-center pt-4 mb-5 pb-5">
          <svg width={200} height={200}>
            <filter
              id="innerShadow"
              x="-20%"
              y="-20%"
              width="140%"
              height="140%"
            >
              <feGaussianBlur
                in="SourceGraphic"
                stdDeviation={3}
                result="blur"
              />
              <feOffset in="blur" dx="2.5" dy="2.5" />
            </filter>
            <g>
              <circle
                id="shadow"
                style={{ fill: "rgba(0,0,0,0.1)" }}
                cx={97}
                cy={100}
                r={87}
                filter="url(#innerShadow)"
              />
              <circle
                id="circle"
                style={{ stroke: "#FFF", strokeWidth: "12px", fill: "#20B7AF" }}
                cx={100}
                cy={100}
                r={80}
              />
            </g>
            <g>
              <line
                x1={100}
                y1={100}
                x2={100}
                y2={55}
                transform="rotate(80 100 100)"
                style={{ strokeWidth: "3px", stroke: "#fffbf9" }}
                id="hourhand"
              >
                <animateTransform
                  attributeName="transform"
                  attributeType="XML"
                  type="rotate"
                  dur="43200s"
                  repeatCount="indefinite"
                />
              </line>
              <line
                x1={100}
                y1={100}
                x2={100}
                y2={40}
                style={{ strokeWidth: "4px", stroke: "#fdfdfd" }}
                id="minutehand"
              >
                <animateTransform
                  attributeName="transform"
                  attributeType="XML"
                  type="rotate"
                  dur="3600s"
                  repeatCount="indefinite"
                />
              </line>
              <line
                x1={100}
                y1={100}
                x2={100}
                y2={30}
                style={{ strokeWidth: "2px", stroke: "#C1EFED" }}
                id="secondhand"
              >
                <animateTransform
                  attributeName="transform"
                  attributeType="XML"
                  type="rotate"
                  dur="60s"
                  repeatCount="indefinite"
                />
              </line>
            </g>
            <circle
              id="center"
              style={{ fill: "#128A86", stroke: "#C1EFED", strokeWidth: "2px" }}
              cx={100}
              cy={100}
              r={3}
            />
          </svg>
        </div>
      </>
    );
  }
}
export default Clock;
