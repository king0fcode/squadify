import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "jquery/dist/jquery.min";

function Footer() {
  return (
    <div className="col col-12 text-center pt-4 mb-5 pb-5">
      <a href="/admin/dashboard">
        <span>Go to Dashboard</span>
      </a>
    </div>
  );
}

export default Footer;
