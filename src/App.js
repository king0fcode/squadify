import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "jquery/dist/jquery.min";

import AdminLayout from "./layouts/Admin";
import ClockVariable from "./variables/clock";

const hist = createBrowserHistory();
function App() {
  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <Router history={hist}>
            <Switch>
              <Route
                exact
                path="/"
                render={props => <ClockVariable {...props} />}
              />

              <Route
                path="/admin"
                render={props => <AdminLayout {...props} />}
              />
            </Switch>
          </Router>
        </div>
      </div>
    </React.Fragment>
  );
}

export default App;
