import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";

import "./index.css";
import AdminLayout from "./layouts/Admin";
import ClockVariable from "./variables/clock";
import * as serviceWorker from "./serviceWorker";
import "./assets/css/steamify.css";
import "./assets/demo/demo.css";
import "./assets/css/nucleo-icons.css";
import "bootstrap/dist/css/bootstrap.css";
import "jquery/dist/jquery.min";
const hist = createBrowserHistory();

// ReactDOM.render(<App />, document.getElementById("root"));
ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/" render={props => <ClockVariable {...props} />} />
      <Redirect from="/" to="/admin/dashboard" />
    </Switch>
  </Router>,
  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
