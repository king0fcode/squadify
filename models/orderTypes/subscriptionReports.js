const mongoose = require("mongoose");
const subscriptionReportsSchema = mongoose.Schema({});

module.exports = mongoose.model(
  "subscriptionReports",
  subscriptionReportsSchema
);
