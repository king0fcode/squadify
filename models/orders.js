const mongoose = require("mongoose");
const orderSchema = mongoose.Schema({
  OrderID: {
    type: Number,
    required: true,
    unique: true
  },
  order_key: {
    type: String,
    required: true,
    unique: true
  },
  created_via: {
    type: String,
    default: "checkout"
  },
  date_created_gmt: {
    type: Date,
    default: Date.now()
  },
  date_modified_gmt: {
    type: Date,
    default: Date.now()
  },
  total: {
    type: String,
    required: true
  },
  customer_id: {
    type: Number,
    required: true
  },
  customer_name: {
    type: String,
    required: true
  },
  customer_address: {
    type: String,
    required: true
  },
  customer_country: {
    type: String,
    required: true
  },
  customer_email: {
    type: String,
    required: true
  },
  payment_method: {
    type: String,
    default: "paypal"
  },
  transaction_id: {
    type: String,
    default: "0"
  },
  payment_date: {
    type: Date,
    default: Date.now()
  },
  product: {
    type: [
      {
        product_id: Number,
        name: String,
        quantity: Number,
        steamUrl: String
      }
    ]
  }
});

module.exports = mongoose.model("Order", orderSchema);

// product_id: {
//   type: Number
// },
// product_name: {
//   type: String
// },
// quantity: {
//   type: Number
// },
// steamUrl: {
//   type: String
// }
