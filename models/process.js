const mongoose = require("mongoose");
const processSchema = mongoose.Schema({
  orderID: {
    type: Number,
    required: true
  },
  processType: {
    typeID: {
      type: String,
      required: true
    },
    typeName: {
      type: String,
      required: true
    }
  },
  steamLink: {
    type: String,
    required: true
  },
  steamID: {
    type: Number,
    default: 0
  },
  processedCount: {
    type: Number,
    default: 0
  },
  requestAmount: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    required: true,
    default: "processing"
  },
  dateAdded: {
    type: Date,
    default: Date.now()
  },
  CorrectLink: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("Process", processSchema);
