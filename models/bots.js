const mongoose = require("mongoose");
const botSchema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  sharedSecret: {
    type: String,
    default: 0
  },
  lastCommend: {
    type: Date,
    default: -1
  },
  operational: {
    type: Number,
    default: 1
  },
  commendedUserIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  reportedUserIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmCommentedUserIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmGroupIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmLikedIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmCuratorIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmWorkshopIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  stmWorkshopItemIDs: {
    UserID: {
      type: Number,
      default: 0
    }
  },
  CreatedOn: {
    type: Date,
    default: Date.now
  },
  accountStats: {
    games: {
      subID: {
        type: Number,
        default: 0
      },
      playedHours: {
        type: Number,
        default: 0
      }
    },
    csgoStats: {
      rank: {
        type: String,
        default: "NoRank"
      }
    }
  }
});

module.exports = mongoose.model("Bot", botSchema);
