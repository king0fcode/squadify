const express = require("express");
const router = express.Router();
const path = require("path");
const appRoot = require("app-root-path");
const { check, validationResult } = require("express-validator");
const fs = require("fs");
const Bot = require("../../models/bots");

router.get("/", (req, res) => {
  res.status(200).json({ msg: "Bots Main Route Running" });
  console.log("Bots Main Route Running");
});

/* Get all bot users  */
router.get("/all", async (req, res) => {
  try {
    const bots = await Bot.find().populate();
    if (!bots || !bots.length) {
      console.log("No Bots Found On Bots collection");
      return res.status(404).json({ msg: "No Bots Found on Bots Collection" });
    }
    res.status(200).json(bots);
    // res.json(bots);
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: "Getting Bots Route Error" });
  }
});

/* end GET all bot Users */

/* get a single bot user info */
router.get("/one/:bot_id", async (req, res) => {
  try {
    const botOne = await Bot.findOne({
      username: req.params.username
    });

    res.status(200).json({ botOne });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: "Cannot get Bot Info: Error" });
  }
});
/* End Get single Bot info */

/* add a single bot */

router.post(
  "/addOne",
  [
    check("username", "Name is required")
      .not()
      .isEmpty(),
    check(
      "password",
      "Please enter a passwrod with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  async (req, res) => {
    //console log request
    console.log(req.body);

    //validation exec
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, password } = req.body;
    try {
      /*check bot exist , if exist sends a error */
      let botExist = await Bot.findOne({ username });
      if (botExist) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User Allready Exists" }] });
      }
      /* end of check */

      /* add new user data to a object for save later onto db */
      bot = new Bot({
        username,
        password
      });
      /* end */

      /* registering user by saving to DB and response with success */
      await bot.save();
      res.status(200).json({ msg: "Bot User Added" });
      /* end */
    } catch (err) {
      /* catch any errors on the request and log it */
      console.log(err.message);
      res.status(500).send("Server catch error");
      /* end */
    }
  }
);

/* End add a single Bot */

/* add Bots using username:Password File  */

router.post("/AddBots", async (req, res) => {
  try {
    /* Get File path from req and assign it , Check file exist */
    const { accounts } = req.body;
    let filePath = path.join(appRoot.toString(), accounts);
    console.log(filePath);

    if (!fs.existsSync(filePath)) {
      console.log('Failed to find file at "' + filePath + '"');
      res.status(400).json({
        msg: "Bot User accounts with that " + filePath + " doesn't exists"
      });
    }
    /* End of Check File Exist */

    /* assign Files value to a array */
    let list = [];
    list = fs
      .readFileSync(filePath)
      .toString()
      .trim()
      .split("\n")
      .map(s => s.trim());
    /* End of assign */

    /* if file is empty send a error message with file with 0 account cannot be added */
    if (list.length <= 0) {
      console.log("Cannot insert zero accounts");
      res.status(500).json({ msg: "cannot insert zero accounts" });
    }
    /* End of Check zero accounts */

    /* start split accounts and add to DB */
    let countA = 0;
    let countB = 0;
    let countC = 0;

    if (typeof list[0] === "string") {
      list = list.map(async s => {
        let parts = s.split(":");
        let username = parts.shift();
        let password = parts.join(":");

        profile = new Bot();

        if (username) profile.username = username;
        if (password) profile.password = password;

        / * check weather user exist in db */;
        try {
          let bot = await Bot.findOne({ username });
          if (bot) {
            countA++;
            console.log(`Username ${username} already exist in DB`);
          } else {
            countB++;
            console.log(`New User: ${username} Added to the DB`);
            await profile.save();
          }
        } catch (err) {
          countC++;
          /* catch any errors on the request and log it */
          console.log(err.message);
          res.status(500).send("Server catch error");
          /* end */
        }

        /* End Check user exist */

        return {
          username: username.trim(),
          password: password.trim(),
          sharedSecret: "",
          countA: countA,
          countB: countB,
          countC: countC
        };
      });
    }

    let msg = `${countA} Users Allready Exist In DB, ${countB} new Users Added to DB || ${countC} errors occures while processing`;
    res.status(200).json(msg);
    /* End Of User Date Save to DB */
  } catch (err) {
    /* catch any errors on the request and log it */
    console.log(err.message);
    res.status(500).send("Server catch error");
    /* end */
  }
});

/* End of Add bots Using username:password File */

/* delete a bot user */
router.delete(
  "/deleteOne/",
  [
    check("username", "Username is required")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);

    const { username } = req.body;
    try {
      /* see if user exist send error */
      let botExist = await Bot.findOne({ username });
      if (!botExist) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User Allready Exists" }] });
      }
      /* End if user Exist send error */

      /* delete Bot user and save back DB */
      await Bot.findOneAndRemove({ username: username });
      await Bot.save();
      /* End */

      res.json({ msg: "User Deleted" });
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Server error");
    }
  }
);
/* End delete a bot */

/* delete all bot users on the DB */

router.delete("/delete/all", async (req, res) => {
  try {
    await Bot.deleteMany((err, result) => {
      if (err) {
        console.log("error query on delete all");
      } else {
        console.log(result);
      }
    });
    console.log(`Users Deleted`);
    res.status(200).json({ msg: `Users Deleted` });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: `Server error` });
  }
});
/* End Delete all bot users on DB */

module.exports = router;
