const express = require("express");
const router = express.Router();
const Orders = require("../../models/orders");
const path = require("path");
const appRoot = require("app-root-path");
const { check, validationResult } = require("express-validator");
const fetch = require("node-fetch");
const freeCommends = require("../../models/orderTypes/freeCommends");
const Process = require("../../models/process");
const config = require("config");
/* initializing woocomerce api */

const WooCommerceAPI = require("woocommerce-api");
const WooCommerce = new WooCommerceAPI({
  url: "https://csgo-commends.me",
  consumerKey: "ck_85b79a45839fc62f1433ba91c292c8a80b04c0a5",
  consumerSecret: "cs_06ee3202fec72da5b4059bca6c33baeb0dec9935",
  wpAPI: true,
  version: "wc/v3"
});

router.get("/", (req, res) => {
  res.status(200).json({ msg: "Process Route running" });
  console.log("Process Route running");
});

router.get("/getAll", async (req, res) => {
  try {
    const process = await Process.find().populate();
    if (!process || !process.length) {
      console.log("No Process Found On Order collection");
      return res
        .status(404)
        .json({ msg: "No Process Found on Process Collection" });
    }
    res.status(200).json(process);
    // res.json(bots);
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: "Getting Process Route Error" });
  }
});
module.exports = router;
