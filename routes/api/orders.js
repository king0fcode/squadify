const express = require("express");
const router = express.Router();
const Orders = require("../../models/orders");
const path = require("path");
const appRoot = require("app-root-path");
const { check, validationResult } = require("express-validator");
const fetch = require("node-fetch");
const freeCommends = require("../../models/orderTypes/freeCommends");
const Process = require("../../models/process");
const config = require("config");
/* initializing woocomerce api */
const jsonfy = require("jsonfy");
let resMsg = {};
const WooCommerceAPI = require("woocommerce-api");
const WooCommerce = new WooCommerceAPI({
  url: "https://csgo-commends.me",
  consumerKey: "ck_85b79a45839fc62f1433ba91c292c8a80b04c0a5",
  consumerSecret: "cs_06ee3202fec72da5b4059bca6c33baeb0dec9935",
  wpAPI: true,
  version: "wc/v3"
});

var Steam = require("steam"),
  steamClient = new Steam.SteamClient(),
  steamUser = new Steam.SteamUser(steamClient),
  steamGC = new Steam.SteamGameCoordinator(steamClient, 730),
  csgo = require("csgo"),
  CSGO = new csgo.CSGOClient(steamUser, steamGC, false);

const SteamAPI = require("steamapi");
const Steamkey = process.env.STEAM_API_KEY;
const steam = new SteamAPI(Steamkey);
const SteamApi = require("web-api-steam");
var SteamID = require("steamid");

//testing woocomerce api

/* end init Woocomerce API */

router.get("/", async (req, res) => {
  //res.status(200).json({ msg: "Orders Route running" });
  // let accID = await CSGO.ToAccountID("76561198810156340");
  // let info = await CSGO.playerProfileRequest(accID);
  // console.log(info);
  let sid = new SteamID("76561198810156340");
  let accountId = sid.getSteamID64();
  let accID = CSGO.ToAccountID(accountId);

  console.log("Orders Route running");
  console.log(accID);

  // res.status(200).json({ msg: resCsgo });
});

router.post("/", (req, res) => {
  // console.log(req.body);
});

/* Get all Orders */
router.get("/all", async (req, res) => {
  try {
    const orders = await Orders.find().populate();
    if (!orders || !orders.length) {
      console.log("No Orders Found On Order collection");
      return res
        .status(404)
        .json({ msg: "No Orders Found on Orders Collection" });
    }
    res.status(200).json(orders);
    // res.json(bots);
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: "Getting Orders Route Error" });
  }
});

/* end GET all Orders  */

/* get a single Order by ID */
router.get("/one/:order_id", async (req, res) => {
  try {
    const orderOne = await Orders.findOne({
      OrderID: req.params.orderID
    });
    /* see if user exist send error */

    if (!orderOne) {
      return res.status(400).json({ msg: "No Order with Give ID Exists" });
    }
    /* End if user Exist send error */
    res.status(200).json({ orderOne });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ msg: "Cannot get Order Info: Server Error" });
  }
});
/* End of Get single Order by ID  */

/* Post a single Order */

router.post(
  "/add",
  [
    check("id", "Order ID is required")
      .not()
      .isEmpty(),
    check("order_key", "Order key is required")
      .not()
      .isEmpty(),
    check("created_via", "created_via  is required")
      .not()
      .isEmpty(),
    check("status", "Order Status is required")
      .not()
      .isEmpty(),
    check("total", "Order total is required")
      .not()
      .isEmpty(),
    check("customer_id", "customer_id  is required")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    //console.log(req.body);
    //validation exec
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const orderData = new Orders(
        ({
          order_key,
          created_via,
          customer_id,
          status,
          date_created_gmt,
          date_modified_gmt,
          total,
          payment_method,
          transaction_id
        } = req.body)
      );

      orderData.OrderID = req.body.id;

      orderData.customer_name =
        req.body.billing.first_name + " " + req.body.billing.last_name;

      orderData.customer_address =
        req.body.billing.address_1 + " " + req.body.billing.address_2;
      orderData.customer_email = req.body.billing.email;
      orderData.customer_country = req.body.billing.country;
      orderData.payment_date = req.body.date_paid;

      list = req.body.line_items;

      list = list.map(s => {
        /* to code order_types section */
        // 1. get the product category to a const and assign it to order_types object
        const productData = WooCommerce.get(
          "products/1010",
          async (request, response) => {
            try {
              const data = JSON.parse(response.body);
              //   console.log(orderData);
              //res.status(200).json(data.categories);

              // get Order types from Config Json File
              const orderTypes = await config.get("OrderTypes");
              const catID = data.categories[0].id.toString();
              const processJson = orderTypes[0][`${catID}`];

              // End Get Order Types

              /* order_types End section */

              // process model object assigns
              const processData = new Process();
              processData.processType.typeID = processJson["processTypeID"];
              processData.processType.typeName = processJson["name"];
              processData.orderID = orderData.OrderID;

              processData.status = "Processing";

              /* Get current steam started count depend on CASE catID  */
              console.log(catID);

              switch (`${catID}`) {
                case "53": {
                }
                case "47": {
                }
                case "129": {
                  /* free Commends Case*/
                  processData.requestAmount = 50;
                  const validUser = await steam
                    .resolve(`${s.meta_data[0].value}`)
                    .catch(err => {
                      console.log(err.message);
                    });
                  if (validUser) {
                    console.log(validUser);
                    let sid = new SteamID(validUser);
                    let steam64id = sid.getSteamID64();
                    processData.steamID = steam64id;
                    // let key = process.env.STEAM_API_KEY;
                    processData.CorrectLink = true;
                    processData.steamLink = `https://steamcommunity.com/profiles/${steam64id}`;
                    console.log(processData.steamLink);
                    resMsg.validUser = true;
                  }

                  /* End free commend Case */
                }
                case "59": {
                }
                case "55": {
                }
                case "56": {
                }
                case "57": {
                }
                case "54": {
                }
                case "61": {
                }
                case "58": {
                }
                case "129": {
                }
                case "60": {
                }
                case "null": {
                }
              }

              /* save process Order Data on process schema */

              await processData.save(function(err) {
                if (err) console.log(err);
                console.log("New Order ProcessData going to add");
                resMsg.ProcessData = processData;
                // console.log(processData);
              });
              /* End of save process data */

              return {
                type_id: data.categories[0].id,
                type_name: data.categories[0].name
              };
            } catch (err) {
              console.log("server error");
              //res.status(500).json({ msg: `server error` });
            }
          }
        );
        // End get the product category to a const and assign it to order_types object
        return {
          product_id: s.id,
          name: s.name,
          quantity: s.quantity,
          steamUrl: s.meta_data[0].value
        };
      });

      orderData.product = list;

      // console.log(orderData);
      let orderExist = false;
      resMsg.OrderID = req.body.id;

      let oid = req.body.id;
      let oKey = req.body.order_key;
      let query = {};
      query = { $or: [{ OrderID: oid }, { order_key: oKey }] };
      await Orders.findOneAndRemove(query, async function(err, data) {
        if (err) console.log(err);
        return true; //return data
      });

      await orderData.save(function(err, data) {
        console.log("tesst");
        if (err) console.log(err);
        // `New Order Successfully added`;
        return res.status(200).json(data);
      });

      // if (!orderExist) {
      //   await orderData.save(function(err, data) {
      //     console.log("tesst");
      //     if (err) console.log(err);
      //     // `New Order Successfully added`;
      //     return res.status(200).json(data);
      //   });
      // }
    } catch (err) {
      /* catch any errors on the request and log it */
      console.log(err.message);
      return res.status(500).send("Server catch error");
      /* end */
    }
  }
);

/* End Post a single Order */

/* 
@route Post api/orders/updateOne 
@desc Update order status
@access private
*/
router.post(
  "/updateOne",
  [
    check("OrderID", "Order ID is required")
      .not()
      .isEmpty(),
    check("status", "Order Status is required")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    //validation exec
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors.array());
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const { OrderID, status } = req.body;
      const order = await Orders.findOne({ OrderID }).populate();
      if (!order) {
        return res
          .status(400)
          .json({ msg: `There is no Order for Order ID ${OrderID}` });
      }
      const updateField = {};
      updateField.status = status;
      updateOrder = await Orders.findOneAndUpdate(
        {
          OrderID
        },
        {
          $set: updateField
        },
        { new: true }
      );
      return res.status(200).json({
        msg: `Order has been updated to ${status} for Order ID : ${OrderID}`
      });
      console.log(order);
    } catch (err) {
      console.log(err.message);
      res.status(500).send("server error");
    }
  }
);
/* End of Update order status */

/* Delete All Orders
@route api/orders/deleteAll 
@desc clear all orders on order collection
@access private 
*/
router.delete("/deleteAll", async (req, res) => {
  try {
    await Orders.deleteMany((err, result) => {
      if (err) {
        console.log("error query on delete all");
        return res
          .status(401)
          .json({ msg: "Error on Delete Orders Collection" });
      } else {
        console.log(result);
        return res.status(200).json({
          msg: "All orders has been Successfully cleared from Orders Collection"
        });
      }
    });
    console.log(`Users Deleted`);
  } catch (err) {
    console.error(err.message);
    res.status(401).json({ msg: "Orders Delete Fails. Server Error" });
  }
});

module.exports = router;
