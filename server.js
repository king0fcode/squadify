const express = require("express");
const connectDB = require("./config/db");
const morgan = require("morgan");
const app = express();
const dotenv = require("dotenv");

connectDB();
dotenv.config({ path: ".env" });
const port = process.env.BACKEND_PORT || 8000;

//require("dotenv/config");

//init middleware
app.use(express.json({ extended: false }));

// dev loggin
// if (process.env.NODE_ENV === "DEV") {
//   app.use(morgan("dev"));
// }
// define routes
const botsRoutes = require("./routes/api/bots");
const ordersRoutes = require("./routes/api/orders");
const processRoutes = require("./routes/api/process");

app.use("/bots", botsRoutes);
app.use("/orders", ordersRoutes);
app.use("/process", processRoutes);

app.get("/", (req, res) => {
  res.send("Back end Running");
});

app.listen(port, () => {
  console.log(`Server Listing on Port: ${port}`);
});
